package nz.evg.ecx.api.json

import nz.evg.ecx.api._
import nz.evg.ecx.api.app.auth.SigninResponse
import nz.evg.ecx.api.app.auth.{SigninResponse, Signin}
import upickle.Js._
import upickle.default.{ReadWriter, _}

object JsonFormats {

  implicit val apiRequestRW = ReadWriter[ApiRequest](
    (value: ApiRequest) ⇒
      Obj(
        "tpe" → (value match {
          case v: Signin ⇒ Str("Signin")
          case v ⇒ throw new IllegalStateException(s"unknown object type: $v")
        }),
        "data" → (value match {
          case s: Signin ⇒ upickle.default.writeJs[Signin](s)
          case v ⇒ throw new IllegalStateException(s"unknown object type: $v")
        })),
    {
      case fields: Obj ⇒
        (fields("tpe"), fields("data")) match {
          case (Str("Signin"), d: Obj) ⇒ upickle.default.readJs[Signin](d)
          case _ ⇒ throw new IllegalArgumentException("unknown request type")
        }
      case v => throw new Exception(s"wrong json value (apiRequest): $v")
    })

  implicit val apiResponseRW = ReadWriter[ApiResponse](
    (value: ApiResponse) ⇒ {
      Obj(
        "tpe" → (value match {
          case s: SigninResponse ⇒ Str("SigninResponse")
          case s: ServerFailure ⇒ Str("ServerFailure")
        }),
        "data" → (value match {
          case s: SigninResponse ⇒ upickle.default.writeJs(s)
          case s: ServerFailure ⇒ upickle.default.writeJs(s)
        })
      )
    },
    {
      case fields: Obj ⇒
        (fields("tpe"), fields("data")) match {
          case (Str("SigninResponse"), data: Obj) ⇒ upickle.default.readJs[SigninResponse](data)
          case (Str("ServerFailure"), data: Obj) ⇒ upickle.default.readJs[ServerFailure](data)
          case tpe ⇒ throw new IllegalArgumentException(s"unknown request type: $tpe")
        }
      case value => throw new Exception(s"wrong json value (apiResponse): $value")
    })
}

