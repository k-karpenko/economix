package nz.evg.ecx

import java.util.UUID

package object domain {
  case class Id[T <: DomainEntity](value: String = UUID.randomUUID().toString)

  sealed trait DomainEntity

  object DomainEntity {

    case class Email(value: String) extends AnyVal

    case class Password(value: String) extends AnyVal

    case class Name(value: String) extends AnyVal


    case class User(id: Option[Id[User]],
                    email: Email,
                    password: Password,
                    name: Name)
      extends DomainEntity

    case class Session(token: String, user: Id[User], timestamp: Long)
      extends DomainEntity

  }

}
