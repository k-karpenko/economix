package nz.evg.ecx.api

sealed trait FailureType extends Throwable
object FailureType {
  case object NotConfigured extends FailureType
  case object AccessDenied extends FailureType
  case object Unknown extends FailureType
  case object Duplicate extends FailureType
  case object WrongPassword extends FailureType
  case object InvalidInput extends FailureType
  case object InvalidOutput extends FailureType
  case object UnknownMethod extends FailureType
  case object Timeout extends FailureType
  case object AlreadyAuthenticated extends FailureType

  case object BarcodeNotFound extends FailureType
  case object RecordNotFound extends FailureType
  case object RoleNotFound extends FailureType
  case object DictionaryNotFound extends FailureType
  case object FormDataNotFound extends FailureType
  case object FormNotFound extends FailureType
  case object FieldNotFound extends FailureType

  case object FileNotResolved extends FailureType
  case object StorageError extends FailureType
  case object AlreadyExist extends FailureType

  case object DataBaseError extends FailureType
  case class Exception(msg: String) extends FailureType

}

