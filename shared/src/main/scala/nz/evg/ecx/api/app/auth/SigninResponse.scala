package nz.evg.ecx.api.app.auth

import nz.evg.ecx.api.{FailureType, ApiResponse}
import nz.evg.ecx.domain.DomainEntity.User

sealed trait SigninResponse extends ApiResponse
object SigninResponse {

  case class SignedIn(user: User, token: String) extends SigninResponse

  case class Failure(failureType: FailureType) extends SigninResponse

}
