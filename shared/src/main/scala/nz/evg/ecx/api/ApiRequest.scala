package nz.evg.ecx.api

import upickle.Js.{Str, Obj}
import upickle.default.ReadWriter

trait ApiRequest {
  type Response <: ApiResponse
}