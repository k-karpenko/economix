package nz.evg.ecx.api.app.auth

import nz.evg.ecx.api.ApiRequestMeta

case object SigninMeta extends ApiRequestMeta[Signin] {
  override val serviceName = "Auth"
  override val methodName = "Signin"
}
