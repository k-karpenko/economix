package nz.evg.ecx.api

import nz.evg.ecx.api.app.auth.SigninMeta

object Meta {
  implicit val signinMeta = SigninMeta
}
