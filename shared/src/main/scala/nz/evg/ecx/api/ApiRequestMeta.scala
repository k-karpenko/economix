package nz.evg.ecx.api

trait ApiRequestMeta[T <: ApiRequest] {
  val serviceName: String
  val methodName: String
}
