package nz.evg.ecx.api.app.auth

import nz.evg.ecx.api.ApiRequest
import nz.evg.ecx.domain.DomainEntity.{Email, Password}

case class Signin(email: Email, password: Password) extends ApiRequest {
  override type Response = SigninResponse
}





