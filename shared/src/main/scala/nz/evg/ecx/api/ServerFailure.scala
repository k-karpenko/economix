package nz.evg.ecx.api

case class ServerFailure( failureType: FailureType) extends ApiResponse
