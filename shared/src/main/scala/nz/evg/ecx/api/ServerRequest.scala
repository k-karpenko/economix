package nz.evg.ecx.api

case class ServerRequest(id: String, service: String, method: String,
                         body: String)
