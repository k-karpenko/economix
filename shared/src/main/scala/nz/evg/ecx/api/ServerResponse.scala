package nz.evg.ecx.api

case class ServerResponse(id: String, data: String)
