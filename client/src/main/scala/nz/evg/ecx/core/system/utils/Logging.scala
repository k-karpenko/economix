package nz.evg.ecx.core.system.utils

import felix.FelixSystem
import slogging.{LogLevel, LoggerConfig, UnderlyingLogger, UnderlyingLoggerFactory}
import vaska._

import scala.concurrent.Future

object Logging {

  def init(system: FelixSystem): Future[Unit] = {
    implicit val ec = system.executionContext
    for {
      app ← system.jsAccess.obj("global").get[JSObj]("App")
      jsLogger ← app.get[JSObj]("Logger")
      _ ← jsLogger.save()
    } yield {
      LoggerConfig.factory = new LoggingFactory(jsLogger)
      LoggerConfig.level = LogLevel.DEBUG
      ()
    }
  }

  private val WARN = 0
  private val DEBUG = 1
  private val ERROR = 2
  private val LOG = 3

  private class LoggingFactory(jsLogger: JSObj) extends UnderlyingLoggerFactory {
    def getUnderlyingLogger(name: String): UnderlyingLogger = {
      new ScalaJsLogger(name.stripPrefix("involve."), jsLogger)
    }
  }

  private class ScalaJsLogger(name: String, jsLogger: JSObj) extends UnderlyingLogger {

    def warn(message: String) = jsLogger.call[Unit]("write", WARN, name, message)

    def warn(message: String, cause: Throwable) = jsLogger.call[Unit]("write", WARN, name, message, cause.toString)

    def warn(message: String, args: AnyRef*) = jsLogger.call[Unit]("write", WARN, name, message.format(args: _*))

    def error(message: String) = jsLogger.call[Unit]("write", ERROR, name, message)

    def error(message: String, cause: Throwable) = jsLogger.call[Unit]("write", ERROR, name, message, cause.toString)

    def error(message: String, args: AnyRef*) = jsLogger.call[Unit]("write", ERROR, name, message.format(args: _*))

    def debug(message: String) = jsLogger.call[Unit]("write", DEBUG, name, message)

    def debug(message: String, cause: Throwable) = jsLogger.call[Unit]("write", DEBUG, name, message, cause.toString)

    def debug(message: String, args: AnyRef*) = jsLogger.call[Unit]("write", DEBUG, name, message.format(args: _*))

    def info(message: String) = jsLogger.call[Unit]("write", LOG, name, message)

    def info(message: String, cause: Throwable) = jsLogger.call[Unit]("write", LOG, name, message, cause.toString)

    def info(message: String, args: AnyRef*) = jsLogger.call[Unit]("write", LOG, name, message.format(args: _*))

    def trace(message: String) = jsLogger.call[Unit]("write", LOG, name, message)

    def trace(message: String, cause: Throwable) = jsLogger.call[Unit]("write", LOG, message, cause.toString)

    def trace(message: String, args: AnyRef*) = jsLogger.call[Unit]("write", LOG, message.format(args: _*))
  }

}
