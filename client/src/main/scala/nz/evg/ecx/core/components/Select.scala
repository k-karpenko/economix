package nz.evg.ecx.core.components

import felix._
import felix.core.FelixSystem.utils._
import felix.core.{EventTarget, FelixSystem}
import felix.vdom.{ElementRef, TextEntry, NodeLike}
import moorka.Rx
import moorka.collection.Buffer
import moorka.rx.{Val, Var}
import nz.evg.ecx.core.system.utils.LoadingState
import slogging.LazyLogging
import vaska.{JSAccess, JSObj}

/**
  * @author Cyril A. Karpenko <self@nikelin.ru>
  */
class Select[T](
                 collection: Seq[T],
                 value: Var[T],
                 converter: (T) => String)
               (implicit val system: FelixSystem) extends Component {

  val selectedIdx = Var(0)

  value once { v =>
    val elementIndex = collection.indexOf(v)
    val idx = if (elementIndex > 0) elementIndex else 0
    selectedIdx pull Val(idx)
  }

  selectedIdx foreach { idx =>
    value pull Val(collection(idx))
  }

  override def start: felix.Element = {
    'select(
      'selectedIndex =:= selectedIdx,
      collection.map(x => 'option(converter(x)))
    )
  }
}

class SelectOptional[T](
                         collection: Rx[LoadingState[Seq[T]]],
                         value: Var[Option[T]],
                         converter: (T) => String,
                         noneElementText: String,
                         xs: NodeLike*)(implicit val system: FelixSystem) extends Component with LazyLogging {

  var element: Element = _

  override def start: Element = {
    'div(
      collection map {
        case LoadingState.Ready(c) ⇒
          element = 'select(
            'change listen {
              element.ref.get[Int]("selectedIndex") map { idx ⇒
                println("Pulled")
                value pull Val(Some(c(idx)))
              }
            },
            'option(noneElementText),
            DataRepeat(Buffer.fromSeq(c), { x: Rx[T] =>
              'option(
                x map { r ⇒ 'span(converter(r)) }
              )
            })
          )
          element
        case LoadingState.Error(throwable) ⇒ 'div("Error: unable to load shows list")
        case _ ⇒ 'div()
      }
    )
  }
}
