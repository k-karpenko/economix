package nz.evg.ecx.core.system

import felix.Component
import felix.vdom.NodeLike
import moorka._
import moorka.rx.{Rx, Val}

import scala.collection.mutable

class PageManager() {
  private val page = Var[Option[NodeLike]](Option.empty)

  val currentPage: Rx[NodeLike] = page.filter{_.nonEmpty}.map{_.get}

  private val pageStack = mutable.Stack[NodeLike]()

  def redirect(newPage: NodeLike): Unit = {
    pageStack.clear()
    pushPageToStack(newPage)
  }

  def pushPageToStack(newPage: NodeLike): Unit = {
    pageStack.push(newPage)
    page pull Val(Some(newPage))
  }

  def popPageFromStack(): Unit = {
    pageStack.pop()
    page pull Val(Some(pageStack.head))
  }
}
