package nz.evg.ecx.core.system

import felix._
import nz.evg.ecx.client.services.{Connection, WSConnection}
import nz.evg.ecx.core.system.utils.Logging
import vaska.JSAccess

import scala.concurrent.duration._

import scala.concurrent.ExecutionContext

object EcxSystem {

  final class Inject(implicit ec: ExecutionContext) {

    val pageManager = new PageManager()

    val serverUrl = "ws://localhost:9010/"

    val connection = new WSConnection(Connection.Config(serverUrl, 20.seconds))
  }

}

class EcxSystem(jsa: JSAccess) extends FelixSystem {
  import EcxSystem._

  val jsAccess = jsa
  implicit val executionContext =
    scala.scalajs.concurrent.JSExecutionContext.Implicits.runNow

  Logging.init(this)
  val inject = new Inject()
}
