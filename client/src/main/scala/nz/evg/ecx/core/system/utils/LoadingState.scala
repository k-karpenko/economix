package nz.evg.ecx.core.system.utils

sealed trait LoadingState[+T]

object LoadingState {

  def default[T]: LoadingState[T] = Loading

  case object Loading extends LoadingState[Nothing]
  case class Ready[+T](data: T) extends LoadingState[T]
  case class Error(throwable: Throwable) extends LoadingState[Nothing]
}
