package nz.evg.ecx.client.views.user

import felix._
import felix.vdom.{NodeLike, Component}
import moorka.rx.{Signal, Val}
import nz.evg.ecx.core.system.EcxSystem
import nz.evg.ecx.domain.DomainEntity.User

object EnterpriseCreationView {

  sealed trait Step
  object Step {
    case object Initial extends Step
    case object Location extends Step
    case object Finalisation extends Step
  }

}

class EnterpriseCreationView(user: User, goBackSignal: Signal)(implicit val system: EcxSystem) extends Component {
  import EnterpriseCreationView._

  val step = Val[Step](Step.Initial)

  override def start: NodeLike = {
    'form(
      'h1(
        'textContent := step map { r ⇒
          "Enterprise - " + (r match {
            case Step.Initial ⇒ "Step #1"
            case Step.Location ⇒ "Step #2"
            case Step.Finalisation ⇒ "Step #3"
          })
        }
      ),
      'button( "Cancel",
        'click listen {
          goBackSignal.fire()
        }
      )
    )
  }

}
