package nz.evg.ecx.client.views

import felix._
import felix.vdom.NodeLike
import felix.{FelixSystem, Component}
import moorka.rx.Channel
import nz.evg.ecx.client.views.user.{EnterpriseCreationView, EnterprisesView}
import nz.evg.ecx.core.system.EcxSystem
import nz.evg.ecx.domain.DomainEntity.User
import slogging.LazyLogging

class WelcomeView(user: User)(override implicit val system: EcxSystem)
  extends Component with LazyLogging {

  private val createSignal = Channel.signal()
  private val goBackSignal = Channel.signal()
  private val listSignal = Channel.signal()

  createSignal map { _ ⇒
    system.inject.pageManager.pushPageToStack(new EnterpriseCreationView(user, listSignal))
  }

  goBackSignal map { _ ⇒
    system.inject.pageManager.pushPageToStack(this)
  }

  listSignal map { _ ⇒
    system.inject.pageManager.pushPageToStack(new EnterprisesView(user, createSignal, goBackSignal))
  }

  override def start: NodeLike =
    'div(
      'h1(s"Welcome, ${user.name.value}!"),
      'ul(
        'li(
          'button(
            "Enterpises",
            'click listen {
              listSignal.fire()
            }
          )
        )
      )
    )
}
