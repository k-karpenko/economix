package nz.evg.ecx.client.services

import java.util.UUID

import moorka.Rx
import moorka.collection.Buffer
import moorka.rx._
import nz.evg.ecx.api._
import nz.evg.ecx.api.json.JsonFormats
import nz.evg.ecx.core.system.utils.LoadingState
import org.scalajs.dom._
import slogging.LazyLogging

import scala.concurrent.ExecutionContext

object WSConnection {

  trait WebSocketAPI {
    var onmessage: MessageEvent ⇒ Unit
    var onopen: Event ⇒ Unit
    var onerror: ErrorEvent ⇒ Unit
    var oncloseCloseEvent: () ⇒ Unit
  }

  sealed trait ConnectionState
  object ConnectionState {
    case object Error extends ConnectionState
    case object Disconnected extends ConnectionState
    case object Connected extends ConnectionState
  }

}

class WSConnection(config: Connection.Config) extends Connection
  with LazyLogging {
  import WSConnection._
  import JsonFormats._
  import upickle.default._

  private val connection = createWebSocket

  def createWebSocket =
    try {
      new WebSocket(config.endpointUrl)
    } catch {
      case e: Throwable ⇒
        e.printStackTrace()
        throw e
    }

  private val state = Var[ConnectionState](ConnectionState.Disconnected)

  state map { va ⇒
    console.log("WS state changed")
    console.log(va.toString)
  }

  private val requests = Channel[ServerRequest]()
  private val responses = Channel[ServerResponse]()

  requests map { request ⇒
    logger.info("Sending request", request)
    connection.send(write(request))
  }

  connection.onmessage = (evt: MessageEvent) ⇒ {
    logger.info("On message")
    console.log("Data")
    console.log(evt)
    responses pull Val(read[ServerResponse](evt.data.toString))
  }

  connection.onopen = (evt: Event) ⇒ {
    logger.info("On open")
    state pull Val(ConnectionState.Connected)
  }

  connection.onerror = (evt: Event) ⇒ {
    logger.info("On error")
    state pull Val(ConnectionState.Error)
  }

  override def request[Request <: ApiRequest](request: Request)
                                             (implicit ec: ExecutionContext,
                                              meta: ApiRequestMeta[Request]): Rx[LoadingState[Request#Response]] = {

    FSM(LoadingState.default[Request#Response]) {
      case LoadingState.Loading ⇒
        console.log("Loading?")
        val requestId = UUID.randomUUID().toString
        val requestBody = write[ApiRequest](request)

        val req = ServerRequest(requestId, meta.serviceName, meta.methodName,
          requestBody)

        val result = responses.filter(_.id == requestId) flatMap { response ⇒
          logger.info("Response received", response.toString)
          Val(
            LoadingState.Ready(read[ApiResponse](response.data).asInstanceOf[Request#Response])
          )
        }

        requests pull Val(req)

        result
      case _ ⇒ Dummy
    }
  }
}
