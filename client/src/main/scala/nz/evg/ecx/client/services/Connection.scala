package nz.evg.ecx.client.services

import moorka.Rx
import nz.evg.ecx.api.{ApiRequestMeta, ApiRequest}
import nz.evg.ecx.core.system.utils.LoadingState

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.FiniteDuration

object Connection {
  case class Config(endpointUrl: String, timeout: FiniteDuration)
}

trait Connection {

  def request[Request <: ApiRequest](request: Request)
                                    (implicit ec: ExecutionContext,
                                     meta: ApiRequestMeta[Request]): Rx[LoadingState[Request#Response]]

}
