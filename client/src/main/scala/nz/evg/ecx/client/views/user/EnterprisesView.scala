package nz.evg.ecx.client.views.user

import felix._
import felix.vdom.{NodeLike, Component}
import moorka.rx.Signal
import nz.evg.ecx.core.system.EcxSystem
import nz.evg.ecx.domain.DomainEntity.User

class EnterprisesView(user: User, onCreateRequested: Signal, onBackRequested: Signal)
                     (implicit val system: EcxSystem) extends Component {

  override def start: NodeLike = {
    'div(
      'h1("Enterprises list"),
      'ul(
        'li("#1")
      ),
      'div(
        'button(
          "Create new one",
          'click listen {
            onCreateRequested.fire()
          }
        ),
        'button(
          "Back",
          'click listen {
            onBackRequested.fire()
          }
        )
      )
    )
  }

}
