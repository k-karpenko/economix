package nz.evg.ecx.client

import moorka.rx.{Channel,  Var}
import nz.evg.ecx.core.system.EcxSystem
import nz.evg.ecx.client.views._
import nz.evg.ecx.domain.DomainEntity.User
import slogging.LazyLogging
import vaska.JSObj
import felix._

/**
  * @author Cyril A. Karpenko <self@nikelin.ru>
  */
@scala.scalajs.js.annotation.JSExport
object Main extends LazyLogging {

  @scala.scalajs.js.annotation.JSExport
  def main(jsa: vaska.JSAccess): Unit = {
    implicit val system = new EcxSystem(jsa)
    implicit val ec = system.executionContext

    val signedIn = Channel[User]()

    def signupView() = new SignupView
    def signinView() = new SigninView(signedIn)

    signedIn map { user ⇒
      system.inject.pageManager.pushPageToStack(new WelcomeView(user))
    }

    val navigationView = new NavigationView(
      NavigationView.Config(
        signinViewRequested = () ⇒ {
          system.inject.pageManager pushPageToStack signinView()
        },
        signupViewRequested = () ⇒ {
          system.inject.pageManager pushPageToStack signupView()
        }
      )
    )

    val el = 'div(
      navigationView,
      system.inject.pageManager.currentPage
    )

    system.document.getAndSaveAs("body", "startupBody") foreach { body ⇒
      jsa.request[Unit]("init") foreach { _ ⇒
        body.call[JSObj]("appendChild", el.ref) foreach { _ ⇒
          body.free()
        }
      }
    }
  }

}