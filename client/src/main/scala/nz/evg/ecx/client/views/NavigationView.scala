package nz.evg.ecx.client.views

import felix.{Element, Component, FelixSystem}
import felix._
import slogging.LazyLogging

object NavigationView {

  case class Config(signinViewRequested: () ⇒ Unit,
                    signupViewRequested: () ⇒ Unit)

}

class NavigationView(config: NavigationView.Config)
                    (implicit val system: FelixSystem)
  extends Component with LazyLogging {

  override def start: Element = {
    'div( 'class := "nav",
      'button(
        "Signin",
        'click listen {
          config.signinViewRequested()
        }
      ),
      'button(
        "Signup",
        'click listen {
          config.signupViewRequested()
        }
      )
    )
  }

}