package nz.evg.ecx.client.views

import felix._
import felix.vdom._
import felix.vdom.NodeLike
import felix.Component
import moorka.rx._
import nz.evg.ecx.api
import nz.evg.ecx.api.app.auth.SigninResponse
import nz.evg.ecx.api.app.auth.{SigninResponse, Signin}
import nz.evg.ecx.core.system.EcxSystem
import nz.evg.ecx.core.system.utils.LoadingState
import nz.evg.ecx.domain.DomainEntity.{User, Password, Email}
import slogging.LazyLogging

class SigninView(signedIn: Channel[User])(override implicit val system: EcxSystem)
  extends Component with LazyLogging {
  import api._
  import Meta._

  // private rx's
  private val email = Var[String]("")
  private val password = Var[String]("")

  private val processing = Var[Boolean](false)

  private val valid =
    for {
      emailValue ← email
      passwordValue ← password
    } yield {
      !emailValue.isEmpty && !passwordValue.isEmpty
    }

  private val failure = Var[Option[FailureType]](None)

  // public signals
  private val signedInListener = Channel[(Email, Password)]()

  signedInListener map { case (userEmail, userPassword) ⇒
    logger.info("Received signin request")
    system.inject.connection.request(Signin(userEmail,
      userPassword)) map {
      case LoadingState.Ready(SigninResponse.SignedIn(user, token)) ⇒
        logger.info("Ready Signed")
        failure pull Val(None)
        processing pull Val(false)
        signedIn pull Val(user)
      case LoadingState.Ready(SigninResponse.Failure(f)) ⇒
        logger.info("Ready Failure")
        failure pull Val(Some(f))
        processing pull Val(false)
      case LoadingState.Loading ⇒
        logger.info("Loading")
        processing pull Val(true)
      case LoadingState.Error(e) ⇒
        logger.info("Error")
        failure pull Val(Some(FailureType.Unknown))
    }
  }

  override def start: NodeLike = {
    'form(
      'submit listen onSubmit,
      'label("E-mail"),
      'input( 'type /= "email", 'name /= "email", 'value =:= email,
        'required /= "required"),
      'label("Password"),
      'input( 'type /= "password", 'name /= "password", 'value =:= password,
        'required /= "required"),
      new RxNode(failure map {
        case None ⇒ 'span("")
        case Some(e) ⇒ 'span(s"Signin failed due to server error!")
      }, system),
      'button(
        'textContent := (processing map {
          case false ⇒ "Enter"
          case true ⇒ "Wait..."
        }),
        'disabled :=
          (for {
            isValid ← valid
            isProcessing ← processing
          } yield !isValid || isProcessing)
      )
    )
  }

  private def onSubmit =
    email once { emailValue ⇒
      password once { passwordValue ⇒
        signedInListener pull Val((Email(emailValue), Password(passwordValue)))
      }
    }
}
