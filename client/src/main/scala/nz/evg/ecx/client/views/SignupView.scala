package nz.evg.ecx.client.views

import felix._
import felix.vdom.NodeLike
import felix.{Component, FelixSystem}
import moorka.rx.{Channel, Var}

class SignupView()(override implicit val system: FelixSystem) extends Component {

  val name = Var[String]("")
  val email = Var[String]("")
  val password = Var[String]("")
  val confirmPassword = Var[String]("")

  val signedUp = Channel.signal()

  val isValid = for {
    nameVal ← name
    emailVal ← email
    passwordVal ← password
    confirmPasswordVal ← confirmPassword
  } yield {
    nameVal.isEmpty && emailVal.isEmpty && passwordVal.isEmpty &&
      confirmPasswordVal.isEmpty && passwordVal != confirmPasswordVal
  }

  override def start: NodeLike = {
    'div(
      'h1("Signup"),
      'div(
        'label("E-mail"),
        'input('type /= "text")
      ),
      'button(
        'type /= "signup",
        'disabled := isValid.map(!_),
        'click listen {
          signedUp.fire()
        }
      )
    )
  }
}
