window.App = window.App || {};

(function(App) {
    'use strict';
    App.Logger = function() {

        var WARN = 0,
            DEBUG = 1,
            ERROR = 2,
            LOG = 3;

        return {
            write: function (level, name, message, cause) {
                name = '[' + name + ']';
                // Small note about copy/paste in this code.
                // Browsers doesn't support passing console.*
                // as lambda function. Sad but true.
                if (cause !== undefined) {
                    switch (level) {
                        case WARN: console.warn(name, message, cause); break;
                        case DEBUG: console.debug(name, message, cause); break;
                        case ERROR: console.error(name, message, cause); break;
                        case LOG: console.log(name, message, cause); break;
                    }
                } else {
                    switch (level) {
                        case WARN: console.warn(name, message); break;
                        case DEBUG: console.debug(name, message); break;
                        case ERROR: console.error(name, message); break;
                        case LOG: console.log(name, message); break;
                    }
                }
            }
        }
    }();
})(window.App);