import moorka.ui.plugin.ResourcesPlugin
import org.scalajs.sbtplugin.ScalaJSPlugin
import org.scalajs.sbtplugin.ScalaJSPlugin.AutoImport._
import sbt._
import sbt.Keys._

val commonSettings = Seq(
  version := "0.1.0-SNAPSHOT",
  organization := "nz.evg",
  resolvers += Resolver.url("Moorka", url("http://dl.bintray.com/tenderowls/moorka"))(Resolver.ivyStylePatterns),
  resolvers += "Flexis Thirdparty Snapshots" at "https://nexus.flexis.ru/content/repositories/thirdparty-snapshots",
  resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
  scalaVersion := Dependencies.Versions.scala,
  scalacOptions ++= Seq(  "-deprecation", "-feature", "-Xfatal-warnings", "-language:postfixOps"),
  addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.0-M5" cross CrossVersion.full)
)

lazy val client = project.
  enablePlugins(ResourcesPlugin).
  enablePlugins(ScalaJSPlugin).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= Dependencies.client.value
  ).
  dependsOn(sharedJS, jsonFormatsJS)

lazy val server = project.
  settings(name := "app-backend").
  settings(commonSettings:_*).
  settings(
    libraryDependencies ++= Dependencies.server.value
  ).
  dependsOn(sharedJVM, jsonFormatsJVM)

lazy val shared = crossProject.crossType(CrossType.Pure).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= Dependencies.shared.value
  )
lazy val sharedJS = shared.js
lazy val sharedJVM = shared.jvm

lazy val jsonFormats = crossProject.crossType(CrossType.Pure).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= Dependencies.shared.value
  )
  .dependsOn(shared)

lazy val jsonFormatsJS = jsonFormats.js
lazy val jsonFormatsJVM = jsonFormats.jvm

lazy val root = (project in file(".")).
  aggregate(
    server, client,
    sharedJS, sharedJVM,
    jsonFormatsJS, jsonFormatsJVM
  )