package nz.evg.ecx.server.actors.auth

import akka.actor.Props
import akka.util.Timeout
import nz.evg.ecx.api._
import nz.evg.ecx.api.app.auth.{SigninMeta, SigninResponse, Signin}
import nz.evg.ecx.api.json.JsonFormats
import nz.evg.ecx.api.{ServerRequest, FailureType}
import nz.evg.ecx.domain.DomainEntity._
import nz.evg.ecx.domain.Id
import nz.evg.ecx.server.actors.CommandsHandlingActor
import nz.evg.ecx.server.config.Settings

import scala.concurrent.Future

import scala.util.control.NonFatal

object AuthenticationHandlingActor {

  def props(settings: Settings): Props = {
    Props(classOf[AuthenticationHandlingActor], settings)
  }

  object Result {
    case class Failure(exception: FailureType) extends Response
    case class Resolved(result: Either[FailureType, User]) extends Response
    case class Persisted(result: Either[FailureType, Id[User]]) extends Response
    case class ListResolved(users: Seq[User]) extends Response
    case object Deleted extends Response
  }

  sealed trait Response
  object Response {
    case class Failure(tpe: FailureType) extends Response
    case class SignedIn(user: Id[User], sessionToken: String, name: String, login: String) extends Response
    case class CheckResult(status: Boolean, isExpired: Boolean) extends Response
  }

}

import akka.pattern._
class AuthenticationHandlingActor(settings: Settings)
  extends CommandsHandlingActor {
  import CommandsHandlingActor._
  import AuthenticationHandlingActor._
  import JsonFormats._
  import upickle.default._

  implicit val timeout = Timeout(STATE_TIMEOUT)

  startWith(State.Awaiting, Data.Empty)

  when(State.Awaiting) {
    case Event(req @ ServerRequest(id, SigninMeta.serviceName, SigninMeta.methodName, data), Data.Empty) =>
      Future {
        read[ApiRequest](data) match {
          case request: Signin ⇒
            if (request.email.value == "test@test.com") {
              Result.Resolved(Right(User(Some(Id[User]("123")), Email("test@test.com"), Password("p"), Name("m"))))
            } else {
              Result.Resolved(Left(FailureType.AccessDenied))
            }
          case _ ⇒ Result.Resolved(Left(FailureType.InvalidInput))
        }
      } recover {
        case e if NonFatal(e) ⇒
          logger.error(e.getMessage, e)
          self ! Result.Resolved(Left(FailureType.Unknown))
      } pipeTo self

      goto(State.Working) using Data.WorkingData(req, sender())
  }

  when(State.Working, stateTimeout = STATE_TIMEOUT ) {
    case Event(StateTimeout, wd @ Data.WorkingData(_, originalSender)) ⇒
      originalSender ! SigninResponse.Failure(FailureType.Timeout)
      stop()
    case Event(Result.Resolved(Left(failure)), wd: Data.WorkingData) ⇒
      wd.originalSender ! SigninResponse.Failure(failure)
      stop()

    case Event(Result.Resolved(Right(user)), wd: Data.WorkingData) ⇒
      wd.originalSender ! SigninResponse.SignedIn(user, "123")
      stop()
  }

  initialize()
}
