package nz.evg.ecx.server.actors

import akka.actor.{ActorContext, ActorRef}
import nz.evg.ecx.server.actors.auth.AuthenticationHandlingActor
import nz.evg.ecx.server.config.Settings

trait CommandHandlers {

  protected def authenticationActorRef(settings: Settings)(implicit context: ActorContext): ActorRef = {
    context.actorOf(AuthenticationHandlingActor.props(settings),
      name = s"auth-commands-handling-actor-${Math.random()}")
  }

}
