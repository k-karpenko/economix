package nz.evg.ecx.server.networking

import java.util.UUID

import akka.actor._
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server._
import akka.pattern._
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.typesafe.scalalogging.slf4j.LazyLogging
import nz.evg.ecx.api.{ServerResponse, ServerRequest}
import nz.evg.ecx.server.actors.RootActors
import nz.evg.ecx.server.config.Settings

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success, Try}

trait AppServer {

  def start(): Unit

}

object AppServer extends LazyLogging {

  sealed trait ServerEvent
  object ServerEvent {
    case class ConnectionOpen(id: String, subscriber: ActorRef) extends ServerEvent
    case object ConnectionClosed extends ServerEvent
  }

  implicit val ec = ExecutionContext.global

  def apply(config: Settings)(implicit system: ActorSystem, materializer: ActorMaterializer) = {
    new StandardAppServer(config)
  }

  private[AppServer] class StandardAppServer(val settings: Settings)
                                            (implicit val system: ActorSystem, materializer: ActorMaterializer)
    extends AppServer with LazyLogging with RootActors {
    import Directives._

    override def start(): Unit = {
      logger.info("Starting websocket server..")
      startWebSocketServer()
    }

    private def startWebSocketServer() = {
      val binding = Http().bindAndHandle(
        pathEndOrSingleSlash {
          Directives.handleWebsocketMessages(Sessions.webSocketFlow(settings, UUID.randomUUID().toString, this))
        },
        interface = settings.serverHost,
        port = settings.serverPort)

      binding map { _ =>
        logger.info(s"[Platform5] - Server Started on ${settings.serverHost}:${settings.serverPort}")
      }
    }
  }

}