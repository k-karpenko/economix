package nz.evg.ecx.server.networking

import java.util.UUID

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.model.ws.{Message, TextMessage}
import akka.stream.scaladsl.{Flow, Keep, Sink, Source}
import akka.stream.stage.{Context, PushStage, SyncDirective}
import akka.stream.{ActorMaterializer, OverflowStrategy}
import com.typesafe.scalalogging.slf4j.LazyLogging
import nz.evg.ecx.api.{ServerFailure, FailureType, ServerResponse, ServerRequest}
import nz.evg.ecx.server.actors.RootActors
import nz.evg.ecx.server.config.Settings

import scala.collection.mutable
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

import upickle.default._

object Sessions extends LazyLogging {
  import AppServer.ServerEvent

  var sessionActors = mutable.Map[String, ActorRef]()

  def webSocketFlow(settings: Settings,
                    clientId: String,
                    rootActors: RootActors)
                 (implicit actorSystem: ActorSystem): Flow[Message, Message, _] = {

    implicit val ec: ExecutionContext = actorSystem.dispatcher
    implicit val materializer = ActorMaterializer()

    Flow[Message].collect[Future[ServerRequest]] {
      case r: TextMessage.Strict ⇒
        Try {
          logger.debug(s"$clientId -> Request ----> ${r.text}")
          read[ServerRequest](r.text)
        } match {
          case Success(obj) ⇒
            logger.info(s"[${obj.service}:${obj.method}]")
            Future(obj)
          case Failure(e: Throwable) ⇒
            logger.error(s"Failed to process income request: ${r.text}")
            throw e
        }
      case tm: TextMessage ⇒
        val result = tm.textStream.runWith(Sink.fold("")(_ + _))
        result map { msg ⇒
          Try {
            read[ServerRequest](msg)
          } match {
            case Success(obj) ⇒
              logger.info(s"[${obj.service}:${obj.method}]")
              obj
            case Failure(e: Throwable) ⇒
              logger.error(s"Failed to process income request: $msg")
              throw e
          }
        }
    } via {
      val actor = actorSystem.actorOf(SessionActor.props(settings, clientId, rootActors),
        s"session-$clientId")
      val in = Flow[Future[ServerRequest]].mapAsyncUnordered(1)(identity).to(Sink.actorRef[ServerRequest](actor, ServerEvent.ConnectionClosed))

      val out = Source.actorRef[ServerResponse](bufferSize = 1, OverflowStrategy.fail)
        .mapMaterializedValue {
            actor ! ServerEvent.ConnectionOpen(UUID.randomUUID().toString, _)
        }

      Flow.wrap(in, out)(Keep.none)
    } collect {
      case r: ServerResponse ⇒
        try {
          val jsonData = write(r)
          logger.debug(s"Response <--- $jsonData")
          TextMessage.Strict(jsonData)
        } catch {
          case e: Throwable ⇒
            logger.error("Failed to serialize response into JSON", e)
            TextMessage.Strict(write(ServerResponse(r.id, write(ServerFailure(FailureType.Unknown)))))
        }
    } via reportErrorsFlow
  }

  private[Sessions] def reportErrorsFlow[T]: Flow[T, T, Unit] = {
    Flow[T].transform(() ⇒ new PushStage[T, T] {
      def onPush(elem: T, ctx: Context[T]): SyncDirective = ctx.push(elem)
    })
  }

}
