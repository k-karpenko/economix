package nz.evg.ecx.server.networking

import akka.actor.{ActorRef, FSM, Props, Status}
import akka.util.Timeout
import com.typesafe.scalalogging.slf4j.LazyLogging
import nz.evg.ecx.api.app.auth.{SigninMeta, SigninResponse}
import nz.evg.ecx.api.json.JsonFormats
import nz.evg.ecx.domain.DomainEntity.User
import nz.evg.ecx.server.actors.{CommandHandlers, RootActors}
import nz.evg.ecx.server.config.Settings
import nz.evg.ecx.server.networking.AppServer.ServerEvent

import scala.concurrent.duration._
import scala.language.postfixOps

import nz.evg.ecx.domain._
import nz.evg.ecx.api._

object SessionActor {

  def props(settings: Settings, clientId: String, rootActors: RootActors): Props = {
    Props(classOf[SessionActor], settings, clientId, rootActors)
  }

  private[SessionActor] sealed trait State
  private[SessionActor] object State {
    case object WebRegistration extends State
    case object NotInitialized extends State
    case object Unauthorized extends State
    case object Progress extends State
    case object Authenticated extends State
  }

  private[SessionActor] sealed trait Data
  private[SessionActor] object Data {
    case object Empty extends Data
    case class RegistrationIdle(requestId: String) extends Data
    case class RegistrationRequest(requestId: String, connection: ActorRef) extends Data
    case class Authentication(id: String, connection: ActorRef) extends Data
    case class Connection(connection: ActorRef) extends Data
    case class UserConnection(user: Id[User], connection: ActorRef) extends Data
  }

  sealed trait Request
  object Request {
    case class WebRegistrationMode(requestId: String) extends Request
    case class UserRequest(userId: Id[User], request: ServerRequest) extends Request
    case object GetTerminalContext extends Request
  }

  sealed trait Response
  object Response {
    case class UserResponse(userId: Id[User], response: ServerResponse)
      extends Request
    case object Registered extends Response
  }
}

class SessionActor(settings: Settings, clientId: String, val rootActors: RootActors)
  extends FSM[SessionActor.State, SessionActor.Data]
  with LazyLogging
  with CommandHandlers {
  import upickle.default._
  import JsonFormats._

  implicit val implicitRootActors = rootActors

  import SessionActor._

  private val progressStateTimeout = 3 seconds

  implicit val timeout = Timeout(500.millis)
  implicit val ec = context.dispatcher
  implicit val system = context.system

  startWith(State.NotInitialized, Data.Empty)

  when(State.NotInitialized) {
    case Event(ServerEvent.ConnectionOpen(_, connection), _) ⇒
      goto(State.Unauthorized) using Data.Connection(connection)

    case Event(Request.WebRegistrationMode(id), _) ⇒
      goto(State.WebRegistration) using Data.RegistrationIdle(id) replying Response.Registered
  }

  when(State.Authenticated) {
    case Event(SessionsRegistryActor.Response.UserSessionRegistered, uc: Data.UserConnection) ⇒
      logger.info("[STATUS] - User Session Registered")
      stay()

  }

  when(State.Unauthorized) {
    case Event(req @  ServerRequest(id, SigninMeta.serviceName, SigninMeta.methodName, data), Data.Connection(connection)) ⇒
      authenticationActorRef(settings) ! req
      goto(State.Progress) using Data.Authentication(id, connection)

    case Event(req @  ServerRequest(id, service, method, data), Data.Connection(connection)) ⇒
      logger.info(s"Not allowed operation requested: $service/$method")
      connection ! ServerResponse(id, write(ServerFailure(FailureType.AccessDenied)))
      stay() using Data.Connection(connection)
  }

  when(State.Progress, stateTimeout = progressStateTimeout) {
    case Event(StateTimeout, dc: Data.Connection) ⇒
      goto(State.Unauthorized) using dc

    case Event(response @ SigninResponse.SignedIn(user, token), Data.Authentication(id, connection)) ⇒
      val written =  write(response.asInstanceOf[ApiResponse])
      logger.info(s"response $response $written")
      connection ! ServerResponse(id, written)
      goto(State.Authenticated) using Data.UserConnection(user.id.get, connection)

    case Event(response: SigninResponse.Failure, Data.Authentication(id, connection)) ⇒
      val written = write(response.asInstanceOf[ApiResponse])
      logger.info(s"response $response $written")
      connection ! ServerResponse(id, written)
      goto(State.Unauthorized) using Data.Connection(connection)

    case Event(StateTimeout, Data.Authentication(id, connection)) ⇒
      connection ! ServerResponse(id, write(ServerFailure(FailureType.Timeout)))
      goto(State.Unauthorized) using Data.Connection(connection)

    case e ⇒
      logger.info(e.toString)
      stay()
  }

  whenUnhandled {
    case Event(ServerEvent.ConnectionClosed, _) ⇒
      goto(State.NotInitialized)

    case Event(request@ ServerRequest(id, _, _, _), uc: Data.UserConnection) ⇒
      logger.info(s"(User) Unhandled event on $stateName: $request")
      uc.connection ! ServerResponse(id, write(ServerFailure(FailureType.Unknown)))
      stay()

    case Event(request@ ServerRequest(id, _, _, _), uc: Data.Connection) ⇒
      logger.info(s"(Guest) Unhandled event on $stateName: $request")
      uc.connection ! ServerResponse(id, write(ServerFailure(FailureType.Unknown)))
      stay()

    case Event(Status.Failure(cause), _) ⇒
      logger.error(cause.getMessage, cause)
      stay()

    case Event(msg, _) ⇒
      logger.error(s"[$stateName $stateData] Unhandled: ${msg.getClass} $msg")
      stay()
  }

  onTransition {
    case State.Unauthorized -> State.Progress ⇒
      logger.info("State.Unauthorized -> State.Progress")
    case State.Progress -> State.Unauthorized ⇒
      logger.info("State.Progress -> State.Unauthorized")
    case State.Progress -> State.Authenticated ⇒
      logger.info("State.Progress -> State.Authenticated")
  }

  initialize()
}
