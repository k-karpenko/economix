package nz.evg.ecx.server.actors

import akka.actor.{ActorRef, FSM}
import com.typesafe.scalalogging.slf4j.LazyLogging
import nz.evg.ecx.api.{ApiRequest, FailureType, ServerResponse, ServerRequest}

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success, Try}

object CommandsHandlingActor {
  private[actors] trait State
  private[actors] object State {
    case object Awaiting extends State
    case object Working extends State
  }

  private[actors] trait Data
  private[actors] object Data {
    case object Empty extends Data
    case class WorkingData(req: ServerRequest, originalSender: ActorRef) extends Data
  }
}

trait CommandsHandlingActor extends FSM[CommandsHandlingActor.State, CommandsHandlingActor.Data]
with LazyLogging {
  import CommandsHandlingActor._

  import scala.concurrent.duration._

  val STATE_TIMEOUT = 60.seconds

  implicit val ec: ExecutionContext = context.dispatcher

  startWith(State.Awaiting, Data.Empty)

  def stateSwitch[In](in: In, req: ServerRequest, originalSender: ActorRef): State = {
    goto(State.Working) using Data.WorkingData(req, originalSender)
  }

  whenUnhandled {
    case Event(e: Any, Data.WorkingData(request, senderRef)) ⇒
      logger.error(s"Unknown request on $stateName: " + e)
      senderRef ! Failure(FailureType.Unknown)
      stop()

    case Event(e: Any, _) ⇒
      logger.error(s"Unknown request on $stateName: " + e)
      stop()
  }
}
