package nz.evg.ecx.server

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import nz.evg.ecx.server.config.Settings
import nz.evg.ecx.server.networking.AppServer

object Platform5 {

  implicit val actorSystem = ActorSystem("platform5-system")
  implicit val actorMaterializer = ActorMaterializer()

  val config = new Settings()

  def main( args: Array[String] ): Unit = {

    val serverBinding = AppServer(config)
    serverBinding.start()
  }

}
