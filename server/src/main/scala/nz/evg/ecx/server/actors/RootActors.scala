package nz.evg.ecx.server.actors


import akka.actor.ActorSystem
import com.typesafe.scalalogging.slf4j.LazyLogging
import nz.evg.ecx.server.config.Settings
import nz.evg.ecx.server.networking.SessionsRegistryActor

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration._

trait RootActors extends LazyLogging {

  implicit val system: ActorSystem

  def settings: Settings

  val sessionsRegistryActorRef = system.actorOf(SessionsRegistryActor.props(), name = SessionsRegistryActor.actorName)

}
