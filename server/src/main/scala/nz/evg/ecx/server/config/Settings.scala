package nz.evg.ecx.server.config

import java.util.concurrent.TimeUnit

import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.slf4j.LazyLogging

import scala.concurrent.duration
import scala.concurrent.duration.FiniteDuration


class Settings( config: Config = ConfigFactory.load() ) extends LazyLogging {
  val serverHost = config.getString("platform5.server.host")
  val serverPort = config.getInt("platform5.server.port")
}
