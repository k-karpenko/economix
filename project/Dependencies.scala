import sbt._
import org.scalajs.sbtplugin.ScalaJSPlugin
import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._

object Dependencies {

  object Versions {
    val scala = "2.11.7"
    val akka = "2.3.12"
    val logback = "1.1.3"
  }

  lazy val common = Def.setting(Seq(
    "com.lihaoyi" %%% "upickle" % "0.3.8"
  ))

  lazy val shared = common

  lazy val client = Def.setting(Seq(
    "org.reactivekittens" %%% "felix" % "0.7.0-SNAPSHOT",
    "org.scala-js" %%% "scalajs-dom" % "0.8.1",
    "biz.enef" %%% "slogging" % "0.3"
  ) ++ common.value)

  lazy val server = Def.setting(Seq(
    "com.typesafe.akka" %% "akka-actor" % Versions.akka,
    "com.typesafe.akka" %% "akka-persistence-experimental" % Versions.akka,
    "com.typesafe.akka" %% "akka-http-experimental" % "1.0",
    "com.typesafe.akka" %% "akka-slf4j" % Versions.akka,
    "com.typesafe.akka" %% "akka-testkit" % Versions.akka % "test",
    "org.scalatest"     %% "scalatest" % "2.2.4" % "test",
    "ch.qos.logback"    % "logback-core" % Versions.logback,
    "ch.qos.logback"    % "logback-classic" % Versions.logback,
    "com.typesafe.scala-logging" %% "scala-logging-slf4j" % "2.1.2"
  ) ++ common.value)
}