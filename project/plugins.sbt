logLevel := Level.Warn

addSbtPlugin("com.lihaoyi" % "workbench" % "0.2.3")
addSbtPlugin("org.scala-js" % "sbt-scalajs" % "0.6.4")
addSbtPlugin("com.tenderowls.opensource" % "moorka-resources-plugin" % "0.5.0-SNAPSHOT")